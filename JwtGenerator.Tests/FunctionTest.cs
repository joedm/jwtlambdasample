using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using JwtGenerator;

namespace JwtGenerator.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestToUpperFunction()
        {
            // Just a quick way to do basic debugging , no unit tests exists yet.
            var function = new Function();
            var context = new TestLambdaContext();
            var token = function.FunctionHandler("Basic Y2FsZWJzOjEyMw==", context);
        }

        [Fact]
        public void ProbeDatabase()
        {
            var function = new Function();
            function.GetDatabaseUserRecord("Caleb", "Caleb");
        }
    }
}
