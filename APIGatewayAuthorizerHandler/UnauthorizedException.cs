﻿using System;

namespace APIGatewayAuthorizerHandler
{
    internal class UnauthorizedException : Exception
    {
        public UnauthorizedException() : base ("Unauthorized")
        {
        }

        public UnauthorizedException(Exception innerException) : base("Unauthorized", innerException)
        {
        }
    }
}