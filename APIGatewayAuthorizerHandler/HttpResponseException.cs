﻿using System;
using System.Net.Http;

namespace APIGatewayAuthorizerHandler
{
    internal class HttpResponseException : Exception
    {
        private HttpResponseMessage msg;

        public HttpResponseException()
        {
        }

        public HttpResponseException(HttpResponseMessage msg)
        {
            this.msg = msg;
        }

        public HttpResponseException(string message) : base(message)
        {
        }

        public HttpResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}