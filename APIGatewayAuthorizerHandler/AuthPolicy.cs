﻿using System.Collections;
using System.Collections.Generic;

namespace APIGatewayAuthorizerHandler
{
    public class AuthPolicy
    {
        public string principalId { get; set; }
        public PolicyDocument policyDocument { get; set; }
        public IDictionary<string, string> context { get; set; }
    }

    public class PolicyDocument
    {
        public string Version { get; set; }
        public Statement Statement { get; set; }
    }

    public class Statement
    {
        public string Action { get; set; }
        public string Effect { get; set; }
        public string Resource { get; set; }
    }
}