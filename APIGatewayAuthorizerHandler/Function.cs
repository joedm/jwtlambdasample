using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Amazon.Lambda.Core;
using Microsoft.IdentityModel.Tokens;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace APIGatewayAuthorizerHandler
{
    public class Function
    {
        public AuthPolicy FunctionHandler(TokenAuthorizerContext input, ILambdaContext context)
        {
            if (!input.AuthorizationToken.StartsWith("Bearer", StringComparison.OrdinalIgnoreCase))
                throw new NotSupportedException("Must be Bearer Authurization Header");
                
            try
            {
                string token = input.AuthorizationToken.Split(' ')[1];

                var tokenHandler = new JwtSecurityTokenHandler();
                var symmetricKey = Convert.FromBase64String("856FECBA3B06519C8DDDBC80BB080553");
                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return new AuthPolicy
                {
                    principalId = principal.Claims.FirstOrDefault(x => x.Type == "Username")?.Value,
                    policyDocument = new PolicyDocument
                    {
                        Version = "2012-10-17",
                        Statement = new Statement
                        {
                            Action = "execute-api:Invoke",
                            Effect = "Allow",
                            Resource = "*"
                        }
                    },
                    context = principal.Claims.ToDictionary(key => key.Type, value => value.Value)
                };
            }
            catch
            {
                throw new UnauthorizedException();
            }
        }
    }
}
