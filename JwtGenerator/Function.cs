using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection.Metadata;
using System.Security.Authentication;
using System.Security.Claims;
using System.Text;
using Amazon.Lambda.Core;
using Microsoft.IdentityModel.Tokens;
using ServiceStack.DataAnnotations;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.Dapper;
using ServiceStack.OrmLite.SqlServer;
using ServiceStack.Text;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace JwtGenerator
{
    public class Function
    {
        public const string Secret = "856FECBA3B06519C8DDDBC80BB080553"; // your symmetric

        public static string GenerateToken(string username, int expireMinutes = 20)
        {
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("Username", username),
                    new Claim("Partner", new Guid("12345678-1234-1234-1234-123456789012").ToString()),
                    new Claim("ChildPartners", "*"),
                }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }

        public string FunctionHandler(string authHeader, ILambdaContext context)
        {
            if (string.IsNullOrWhiteSpace(authHeader))
                throw new ArgumentException(nameof(authHeader));

            Credentials credentials = GetCredentialsFromBasicAuth(authHeader);

            if (!AuthenticateGcastUser(credentials.Username, credentials.Password))
                throw new AuthenticationException("Username and/or Password is Invalid");

            return GenerateToken(credentials.Username, 60);
        }

        public Credentials GetCredentialsFromBasicAuth(string authHeaderString)
        {
            string[] splitHeader = authHeaderString.Split(' ');

            if (splitHeader.Length != 2)
                throw new ArgumentException("Authentication Header Format is Not Supported");

            var header = new AuthenticationHeaderValue(splitHeader[0], splitHeader[1]);

            if (!header.Scheme.Equals("Basic", StringComparison.OrdinalIgnoreCase))
                throw new NotSupportedException("Only Basic Authentication Headers are Supported");
            
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string plainTextParams = encoding.GetString(Convert.FromBase64String(header.Parameter));

            string[] splitParams = plainTextParams.Split(':');
            if (splitParams.Length != 2)
                throw new NotSupportedException("Username and Password Parameter Format is Not Supported");

            return new Credentials
            {
                Username = splitParams[0],
                Password = splitParams[1]
            };
        }

        public bool AuthenticateGcastUser(string username, string password)
        {
            return GetDatabaseUserRecord(username, password) != null;
        }

        public class Credentials
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }
        
        public class Users
        {
            [AutoIncrement]
            [PrimaryKey]
            public int Id { get; set; }

            [ServiceStack.DataAnnotations.Index(true, Clustered = true)]
            public string Username { get; set; }

            public string Password { get; set; }
        }

        public Users GetDatabaseUserRecord(string username, string password)
        {
            var dbFactory = new OrmLiteConnectionFactory(
                "Data Source=test-database.c7iycrmexsjl.ap-southeast-2.rds.amazonaws.com,1433;Initial Catalog=userstore;Integrated Security=False;User ID=service;Password=service;Connect Timeout=30",
                SqlServerDialect.Provider);

            using (var db = dbFactory.Open())
            {
                var result = db.Select<Users>(x => x.Username == username && x.Password == password);
                return result.FirstOrDefault();
            }

            //var connection = new SqlConnection("Data Source=test-database.c7iycrmexsjl.ap-southeast-2.rds.amazonaws.com,1433;Initial Catalog=userstore;Integrated Security=False;User ID=service;Password=service;Connect Timeout=30");
            //connection.Open();

            //SqlCommand cmd = new SqlCommand("SELECT * From Users Where Username = @username AND Password = @password", connection);
            //cmd.Parameters.Add(new SqlParameter("username", username));
            //cmd.Parameters.Add(new SqlParameter("password", password));

            //var reader = cmd.ExecuteReader();
            //if (!reader.HasRows)
            //    return null;

            //reader.Read();
            //return new Users
            //{
            //    Username = reader[1] as string,
            //    Password = reader[2] as string
            //};
        }
    }
}
